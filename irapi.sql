-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Nov 30, 2017 at 07:15 PM
-- Server version: 5.5.54-0ubuntu0.14.04.1
-- PHP Version: 5.6.31-4+ubuntu14.04.1+deb.sury.org+4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `irapi`
--

-- --------------------------------------------------------

--
-- Table structure for table `concept`
--

CREATE TABLE IF NOT EXISTS `concept` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` longtext COLLATE utf8_unicode_ci NOT NULL,
  `tag` longtext COLLATE utf8_unicode_ci NOT NULL,
  `url` longtext COLLATE utf8_unicode_ci NOT NULL,
  `domain` longtext COLLATE utf8_unicode_ci NOT NULL,
  `xpath` longtext COLLATE utf8_unicode_ci NOT NULL,
  `created_at` datetime NOT NULL,
  `category` longtext COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=39 ;

--
-- Dumping data for table `concept`
--

INSERT INTO `concept` (`id`, `name`, `tag`, `url`, `domain`, `xpath`, `created_at`, `category`) VALUES
(10, 'name', 'seed', 'http://www.johnnyseeds.com/organic/organic-vegetables/alfalfa-organic-micro-green-seed-2150MG.html?cgid=organic-vegetables#start=1', 'http://www.johnnyseeds.com/organic/organic-vegetables', './/div[@id="pdpMain"]/div[1]/div[1]/div[1]/div[1]/h1[1]', '2017-11-12 17:43:45', 'seed'),
(11, 'species', 'seed', 'http://www.johnnyseeds.com/organic/organic-vegetables/alfalfa-organic-micro-green-seed-2150MG.html?cgid=organic-vegetables#start=1', 'http://www.johnnyseeds.com/organic/organic-vegetables', './/div[@id="pdpMain"]/div[1]/div[1]/div[1]/div[1]/p[1]', '2017-11-12 17:44:21', 'seed'),
(12, 'price', 'seed', 'http://www.johnnyseeds.com/organic/organic-vegetables/alfalfa-organic-micro-green-seed-2150MG.html?cgid=organic-vegetables#start=1', 'http://www.johnnyseeds.com/organic/organic-vegetables', './/div[@id="product-content"]/div[1]/div[1]/div[2]/form[1]/div[2]/span[1]', '2017-11-12 17:44:31', 'seed'),
(13, 'name', 'seed', 'https://www.lowes.com/pd/Ferry-Morse-500-mg-Tomato-Beefstake-L0000/1000223253', 'https://www.lowes.com/pd/Ferry-Morse-500-mg-Tomato-Beefstake-L0000', './/div[@id="main"]/div[3]/section[1]/div[2]/div[1]/h1[1]', '2017-11-12 17:52:23', 'seed'),
(14, 'species', 'seed', 'https://www.lowes.com/pd/Ferry-Morse-500-mg-Tomato-Beefstake-L0000/1000223253', 'https://www.lowes.com/pd/Ferry-Morse-500-mg-Tomato-Beefstake-L0000', './/div[@id="main"]/div[3]/section[1]/div[2]/div[2]/div[1]/p[1]', '2017-11-12 17:54:25', 'seed'),
(15, 'price', 'seed', 'https://www.lowes.com/pd/Ferry-Morse-500-mg-Tomato-Beefstake-L0000/1000223253', 'https://www.lowes.com/pd/Ferry-Morse-500-mg-Tomato-Beefstake-L0000', './/div[@id="main"]/div[3]/section[1]/div[3]/div[1]/div[1]/div[1]/span[1]', '2017-11-12 17:54:38', 'seed'),
(18, 'name', 'text', 'http://www.burpee.com/vegetables/arugula/arugula-roquette-organic-prod000567.html', 'http://www.burpee.com/vegetables/arugula', './/article[@id="pdpMain"]/div[1]/h1[1]', '2017-11-12 22:57:56', 'seed'),
(19, 'species', 'text', 'http://www.burpee.com/vegetables/arugula/arugula-roquette-organic-prod000567.html', 'http://www.burpee.com/vegetables/arugula', './/article[@id="pdpMain"]/div[1]/p[2]', '2017-11-12 22:58:13', 'seed'),
(20, 'price', 'text', 'http://www.burpee.com/vegetables/arugula/arugula-roquette-organic-prod000567.html', 'http://www.burpee.com/vegetables/arugula', './/article[@id="pdpMain"]/div[1]/div[4]/div[2]/div[3]/div[1]/div[1]/div[1]', '2017-11-12 22:58:23', 'seed'),
(21, 'name', 'text', 'http://www.territorialseed.com/product/Jade_II_Bean_Seed', 'http://www.territorialseed.com/product', './/div[@id="content"]/h1[1]', '2017-11-12 23:44:49', 'seed'),
(22, 'species', 'text', 'http://www.territorialseed.com/product/Jade_II_Bean_Seed', 'http://www.territorialseed.com/product', './/div[@id="content"]/section[1]/div[2]/h2[1]', '2017-11-12 23:45:06', 'seed'),
(23, 'price', 'text', 'http://www.territorialseed.com/product/Jade_II_Bean_Seed', 'http://www.territorialseed.com/product', './/div[@id="content"]/section[1]/div[2]/div[2]/div[1]/div[1]/form[1]/ul[1]/li[4]', '2017-11-12 23:45:16', 'seed'),
(24, 'name', 'text', 'https://www.amazon.com/Jobes-Organics-Vegetable-Fertilizer-Vegetables/dp/B001D11M9S', 'https://www.amazon.com/Jobes-Organics-Vegetable-Fertilizer-Vegetables/dp', './/span[@id="productTitle"]', '2017-11-15 18:54:39', 'fertilizer'),
(25, 'requirements', 'text', 'https://www.amazon.com/Jobes-Organics-Vegetable-Fertilizer-Vegetables/dp/B001D11M9S', 'https://www.amazon.com/Jobes-Organics-Vegetable-Fertilizer-Vegetables/dp', './/div[@id="feature-bullets"]/ul[1]/li[2]/span[1]', '2017-11-15 18:55:37', 'fertilizer'),
(26, 'constraints', 'text', 'https://www.amazon.com/Jobes-Organics-Vegetable-Fertilizer-Vegetables/dp/B001D11M9S', 'https://www.amazon.com/Jobes-Organics-Vegetable-Fertilizer-Vegetables/dp', './/div[@id="feature-bullets"]/ul[1]/li[3]/span[1]', '2017-11-15 18:55:54', 'fertilizer'),
(27, 'price', 'text', 'https://www.amazon.com/Jobes-Organics-Vegetable-Fertilizer-Vegetables/dp/B001D11M9S', 'https://www.amazon.com/Jobes-Organics-Vegetable-Fertilizer-Vegetables/dp', './/span[@id="priceblock_ourprice"]', '2017-11-15 18:56:15', 'fertilizer'),
(28, 'name', 'txt', 'http://www.theorganic.life/purna-grow-well-100ml-3560.html', 'http://www.theorganic.life/', './/form[@id="product_addtocart_form"]/div[3]/div[1]/h1[1]', '2017-11-15 19:47:07', 'fertilizer'),
(29, 'requirements', 'txt', 'http://www.theorganic.life/purna-grow-well-100ml-3560.html', 'http://www.theorganic.life/', './/form[@id="product_addtocart_form"]/div[3]/div[2]/div[1]/ul[1]/li[4]/span[1]', '2017-11-15 19:47:29', 'fertilizer'),
(30, 'constraints', 'txt', 'http://www.theorganic.life/purna-grow-well-100ml-3560.html', 'http://www.theorganic.life/', './/form[@id="product_addtocart_form"]/div[3]/div[2]/div[1]/ul[1]/li[5]/span[1]', '2017-11-15 19:47:38', 'fertilizer'),
(31, 'price', 'txt', 'http://www.theorganic.life/purna-grow-well-100ml-3560.html', 'http://www.theorganic.life/', './/span[@id="product-price-3560"]', '2017-11-15 19:47:50', 'fertilizer'),
(32, 'name', 'text', 'https://www.lowes.com/pd/ORTHO-Max-11-5-lb-Fire-Ant-Killer/1015057', 'https://www.lowes.com/pd/ORTHO-Max-11-5-lb-Fire-Ant-Killer', './/div[@id="main"]/div[3]/section[1]/div[2]/div[1]/h1[1]', '2017-11-15 20:26:31', 'pesticide'),
(33, 'technique', 'text', 'https://www.lowes.com/pd/ORTHO-Max-11-5-lb-Fire-Ant-Killer/1015057', 'https://www.lowes.com/pd/ORTHO-Max-11-5-lb-Fire-Ant-Killer', './/div[@id="collapseDesc"]/div[1]/div[1]/p[1]', '2017-11-15 20:26:54', 'pesticide'),
(35, 'price', 'text', 'https://www.lowes.com/pd/ORTHO-Max-11-5-lb-Fire-Ant-Killer/1015057', 'https://www.lowes.com/pd/ORTHO-Max-11-5-lb-Fire-Ant-Killer', './/div[@id="main"]/div[3]/section[1]/div[3]/div[1]/div[1]/div[1]/span[1]', '2017-11-15 20:29:04', 'pesticide'),
(36, 'name', 'txt', 'http://www.easypestsupplies.com.au/antmaster-ant-killer.html', 'http://www.easypestsupplies.com.au/', './/div[@id="content"]/div[1]/div[1]/div[1]/div[3]/div[1]/div[1]/form[1]/div[1]/div[2]/h1[1]', '2017-11-15 20:33:06', 'pesticide'),
(37, 'technique', 'text', 'http://www.easypestsupplies.com.au/antmaster-ant-killer.html', 'http://www.easypestsupplies.com.au/', './/div[@id="content_block_description"]/p[11]', '2017-11-15 20:34:32', 'pesticide'),
(38, 'price', 'text', 'http://www.easypestsupplies.com.au/antmaster-ant-killer.html', 'http://www.easypestsupplies.com.au/', './/span[@id="sec_discounted_price_29783"]', '2017-11-15 20:34:42', 'pesticide');

-- --------------------------------------------------------

--
-- Table structure for table `fos_user`
--

CREATE TABLE IF NOT EXISTS `fos_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(180) COLLATE utf8_unicode_ci NOT NULL,
  `username_canonical` varchar(180) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(180) COLLATE utf8_unicode_ci NOT NULL,
  `email_canonical` varchar(180) COLLATE utf8_unicode_ci NOT NULL,
  `enabled` tinyint(1) NOT NULL,
  `salt` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `last_login` datetime DEFAULT NULL,
  `confirmation_token` varchar(180) COLLATE utf8_unicode_ci DEFAULT NULL,
  `password_requested_at` datetime DEFAULT NULL,
  `roles` longtext COLLATE utf8_unicode_ci NOT NULL COMMENT '(DC2Type:array)',
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_957A647992FC23A8` (`username_canonical`),
  UNIQUE KEY `UNIQ_957A6479A0D96FBF` (`email_canonical`),
  UNIQUE KEY `UNIQ_957A6479C05FB297` (`confirmation_token`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
