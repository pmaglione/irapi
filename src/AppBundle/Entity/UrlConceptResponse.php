<?php

namespace AppBundle\Entity;


class UrlConceptResponse
{
    private $success;
    private $domain;
    private $concepts;

    /**
     * @return mixed
     */
    public function getSuccess()
    {
        return $this->success;
    }

    /**
     * @param mixed $success
     */
    public function setSuccess($success)
    {
        $this->success = $success;
    }

    /**
     * @return mixed
     */
    public function getDomain()
    {
        return $this->domain;
    }

    /**
     * @param mixed $domain
     */
    public function setDomain($domain)
    {
        $this->domain = $domain;
    }

    /**
     * @return mixed
     */
    public function getConcepts()
    {
        return $this->concepts;
    }

    /**
     * @param mixed $concepts
     */
    public function setConcepts($concepts)
    {
        $this->concepts = $concepts;
    }



}