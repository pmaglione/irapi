<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Concept;
use AppBundle\Entity\UrlConceptResponse;
use AppBundle\Form\ConceptType;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\BrowserKit\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends Controller
{
    const REGEX_DOMAIN = '^(?:https?:\/\/)?(?:[^@\n]+@)?(?:www\.)?([^:\/\n]+)';

    /**
     * @Route("/concept", name="concept")
     */
    public function conceptAction(Request $request)
    {
        $form = $this->getConceptForm(null);


        return $this->render("default/concept.html.twig", array("form" => $form->createView()));
    }

    /**
     * @Route("/create_concept", name="create_concept")
     */
    public function crearConceptAction(Request $request)
    {
        $concept = new Concept();
        $form = $this->getConceptForm($concept);



        if ($request->getMethod() == 'POST') {
            $form->handleRequest($request);

            if ($form->isSubmitted() && $form->isValid()) {
                $domain = $this->getDomain($concept->getUrl());
                $concept->setDomain($domain);

                $em = $this->getDoctrine()->getManager();
                $em->persist($concept);
                $em->flush();

                return new JsonResponse(Array(
                    "Success"=>true,
                    "Message"=>"El concepto se registro exitosamente"
                ));
            }
        }


        return new JsonResponse(Array(
            "Success"=>false,
            "Message"=>"Ocurrio un error registrando el concepto",
            "FormValid" => $form->isValid(),
            "Errors" => (string) $form->getErrors(true, false)
        ));
    }

    private function getDomain($url){
        $parts = parse_url($url);
        $domain = $url;

        if (array_key_exists("path", $parts) && array_key_exists("scheme", $parts) && array_key_exists("host", $parts)){
            $queryArray = explode("/", $parts["path"]);
            $queryArray = array_filter($queryArray);
            array_pop($queryArray);

            $query = implode("/", $queryArray);

            $domain = $parts["scheme"] . "://" . $parts["host"] . "/" . $query;
        }

        return $domain;
    }

    private function getConceptForm(Concept $concept = null){
        return $this->get('form.factory')->create(new ConceptType(), $concept);
    }

    /**
     * @Route("/get_concepts", name="get_concepts")
     */
    public function detalleProyectoAction(Request $request)
    {
        $url = $request->query->get("url");
        $domain = $this->getDomain($url);

        $concepts = $this->getDoctrine()->getRepository('AppBundle:Concept')->findByDomain($domain);

        $result = Array(
            "Success" => false,
            "Category" => "",
            "Domain" => $domain,
            "Concepts" => Array()
        );

        if ($concepts == null || sizeof($concepts) == 0 ){
            $response = new JsonResponse($result);

            return $response;
        }else{
            $result["Success"] = true;
            $result["Category"] = $concepts[0]->getCategory();
            $result["Concepts"] = $this->conceptsToArray($concepts);

            $response = new JsonResponse($result);

            return $response;
        }
    }

    private function conceptsToArray($concepts){
        $conceptsArray = Array();
        foreach($concepts as $concept){
            $conceptsArray[] = Array(
                "CreatedAt" => $concept->getCreatedAt()->format("d-m-Y"),
                "Name" => $concept->getName(),
                "Tag" => $concept->getTag(),
                "Xpath" => $concept->getXpath()
            );
        }
        return $conceptsArray;
    }

}
