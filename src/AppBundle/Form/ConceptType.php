<?php

namespace AppBundle\Form;

use AppBundle\Entity\CommentProperty;
use AppBundle\Entity\Concept;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ConceptType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name','text', array(
                'label' => 'Nombre',
                'attr'=>array(
                    'class'=>'full',
                ),
                'required' => true)
            )
            ->add('category','text', array(
                'label' => 'category',
                'attr'=>array(
                    'class'=>'full',
                ),
                'required' => true
            ))
            ->add('tag','text', array(
                'label' => 'tag',
                'attr'=>array(
                    'class'=>'full',
                    ),
                'required' => true
            ))
            ->add('url','text', array(
                'label' => 'url',
                'attr'=>array(
                    'class'=>'full',
                    ),
                'required' => false
            ))
            ->add('xpath','text', array(
                'label' => 'Xpath',
                'attr'=>array(
                    'class'=>'full',
                ),
                'required' => false
            ))
            ;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return '';
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'csrf_protection' => false,
            'data_class' => Concept::class,
        ));
    }
}